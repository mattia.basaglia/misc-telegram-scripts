#!/usr/bin/env python3
import io
import traceback
import subprocess

from telethon.events import NewMessage


from common import ClientCommmon


script = ClientCommmon()

script.parser.add_argument("file")
script.parser.add_argument("emoji")
script.parser.add_argument("--emoji-set", "-s", default="Glaxmoji")

ns = script.parse_args()


stickers_bot_id = 429000
emoji_set = ns["emoji_set"]
client = script.create_client()


async def make_png():
    pipe = subprocess.Popen([
        "convert",
        "-background", "transparent",
        "-gravity", "center",
        "-extent", "512x512",
        "-resize", "100x100",
        ns["file"],
        "png:-"
    ], stdout=subprocess.PIPE)
    out = pipe.communicate()
    emoji_png = io.BytesIO(out[0])
    emoji_png.name = "foo.png"
    await client.send_file(stickers_bot_id, emoji_png, force_document=True)


async def on_message(event):
    try:
        message = event.message.text
        if message == "Choose an emoji set.":
            await client.send_message(entity=stickers_bot_id, message=emoji_set)
        elif message.startswith("Alright! Now send me the custom emoji"):
            await make_png()
        elif "Send me a replacement emoji that corresponds to your custom emoji." in message:
            await client.send_message(entity=stickers_bot_id, message=ns["emoji"])
        elif "There we go" in message:
            await client.send_message(entity=stickers_bot_id, message="/done")
            await client.disconnect()
    except Exception as e:
        traceback.print_exc()
        await client.disconnect()



with script.client:
    client.add_event_handler(on_message, NewMessage(
        chats=[stickers_bot_id], incoming=True, from_users=[stickers_bot_id]
    ))
    client.send_message(entity=stickers_bot_id, message="/addemoji")
    client.run_until_disconnected()
