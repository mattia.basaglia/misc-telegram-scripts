#!/usr/bin/env python3
from common import ClientCommmon


script = ClientCommmon()
script.parser.add_argument("--user-id", type=int)
script.parser.add_argument("--token")
script.parser.add_argument("--message", "-m")

script.parse_args()
#script.args["session"] = "send_message"


client = script.create_client()
client.connect()
client.sign_in(bot_token=script.args["token"])
print(script.args["token"])
print(client.send_message(entity=script.args["user_id"], message=script.args["message"]))
