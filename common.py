import sys
import json
import pathlib
import argparse

import telethon
from telethon.sync import TelegramClient


class ClientCommmon:
    def __init__(self):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument("--api-id")
        self.parser.add_argument("--api-hash")
        self.parser.add_argument("--settings", default=pathlib.Path(__file__).parent / "settings.json", type=pathlib.Path)
        self.parser.add_argument("--session", default="misc_scripts")
        self.add_arguments(self.parser)
        self.args = None
        self.client = None

    def add_arguments(self, parser):
        pass

    def create_client(self):
        if not self.args:
            self.parse_args()

        session = self.args["session"]
        api_id = self.args["api_id"]
        api_hash = self.args["api_hash"]

        self.client = TelegramClient(session, api_id, api_hash)
        return self.client

    def parse_args(self):
        self.args = vars(self.parser.parse_args())

        if self.args["settings"] and self.args["settings"].exists():
            with open(self.args["settings"], "r") as settings_file:
                settings = json.load(settings_file)
            for k, v in self.args.items():
                if not v and k in settings:
                    self.args[k] = settings[k]

        return self.args

    def __enter__(self):
        if not self.client:
            self.create_client()

        self.client.__enter__()
        return self

    def __exit__(self, *a, **kw):
        return self.client.__exit__(*a, **kw)
