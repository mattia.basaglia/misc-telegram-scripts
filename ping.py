#!/usr/bin/env python3
from common import ClientCommmon
from telethon.events import NewMessage


script = ClientCommmon()
script.parser.add_argument("--user-id", type=int)
script.parser.add_argument("--token")
script.parser.add_argument("--prompt", "-p", default="/ping")
script.parser.add_argument("--reply", "-r", default="pong")

ns = script.parse_args()

client = script.create_client()
client.connect()
client.sign_in(bot_token=script.args["token"])


async def on_message(event):
    if event.message.text == ns["prompt"]:
        await client.send_message(entity=event.chat, message=ns["reply"])
        await client.disconnect()


with script.client:
    me = client.get_me()
    if me.username:
        print("@" + me.username)
    print(me.id)
    client.add_event_handler(on_message, NewMessage(incoming=True))
    client.run_until_disconnected()
