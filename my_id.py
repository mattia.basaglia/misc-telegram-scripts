#!/usr/bin/env python3
from common import ClientCommmon


with ClientCommmon() as c:
    me = c.client.get_me()
    if me.username:
        print("@" + me.username)
    print(me.id)
