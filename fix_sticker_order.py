#!/usr/bin/env python3
import pathlib
from telethon import functions
from common import ClientCommmon


def save(filename, sticker_sets):
    with open(filename, "w") as f:
        for sid in sticker_sets:
            f.write("%s\n" % sid)



script = ClientCommmon()
script.parser.add_argument(
    "--save",
    action="store_true",
    help="Saves the current order"
)
script.parser.add_argument(
    "--file",
    type=pathlib.Path,
    default=pathlib.Path(__file__).parent / "sticker_sets",
    help="File used to store the sticker order"
)
script.parser.add_argument(
    "--verbose", "-v",
    action="store_true",
    help="Show what the script is doing"
)


with script:
    sticker_sets = [s.id for s in script.client(functions.messages.GetAllStickersRequest(hash=0)).sets]

    if script.args["save"]:
        if script.args["verbose"]:
            print("Saving")
        save(script.args["file"], sticker_sets)
    else:
        with open(script.args["file"]) as f:
            saved_sticker_sets = list(map(int, f))

        if len(saved_sticker_sets) != len(sticker_sets):
            if script.args["verbose"]:
                print("Sticker sets changed, saving")
            save(script.args["file"], sticker_sets)
        elif saved_sticker_sets != sticker_sets:
            if script.args["verbose"]:
                print("Sorting")
            script.client(functions.messages.ReorderStickerSetsRequest(
                masks=False,
                order=saved_sticker_sets
            ))
        elif script.args["verbose"]:
            print("Nothing to do")
