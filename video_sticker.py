#!/usr/bin/env python
import sys
import json
import shlex
import pathlib
import datetime
import argparse
import fractions
import subprocess


parser = argparse.ArgumentParser()
parser.add_argument("file", type=pathlib.Path)
parser.add_argument("--crop", nargs=4, type=int, default=None, metavar=["WIDTH", "HEIGHT", "X", "Y"])
parser.add_argument("--time-scale", type=float, default=None)
parser.add_argument("--bitrate", default=None)
parser.add_argument("--check", action="store_true", help="Checks if the file is OK as a sticker")


ns = parser.parse_args()


filename = str(ns.file)
probe_cmd = ["ffprobe", "-v", "quiet", "-print_format", "json", "-show_streams", filename]
print("FFmpeg Probe")
print("\t" + " ".join(map(shlex.quote, probe_cmd)))

probe = subprocess.Popen(probe_cmd, stdout=subprocess.PIPE)
info = json.loads(probe.communicate()[0])
for stream in info["streams"]:
    if stream["codec_type"] == "video":
        width = stream["width"]
        height = stream["height"]
        if "duration" not in stream and "DURATION" in stream["tags"]:
            duration = datetime.timedelta(**dict(zip(
                ["hours", "minutes", "seconds"],
                map(float, stream["tags"]["DURATION"].split(":"))
            ))).total_seconds()

        else:
            duration = float(stream["duration"])
        break
else:
    raise Exception("No video streams")


print("Input")
print("\tPath: %s" % filename)
print("\tWidth: %s" % width)
print("\tHeight: %s" % height)
print("\tDuration: %s" % duration)


if ns.check:
    print("Checks")
    def check(name, value, optimal):
        if isinstance(optimal, bool):
            ok = optimal
        elif isinstance(optimal, str):
            ok = value == optimal
        else:
            ok = value <= optimal
        color = "\x1b[31m" if not ok else ""
        print("\t%s%s: %s\x1b[m" % (color, name, value))
        if not ok and not isinstance(optimal, bool):
            print("\t\tOptimal value: %s" % optimal)

    check("Dimensions", (width, height), (width == 512 or height == 512) and width <= 512 and height <= 512)
    check("Duration", duration, 3)
    fps = fractions.Fraction(stream["r_frame_rate"])
    check("FPS", fps, 30)
    with open(ns.file, "rb") as f:
        file_size_k = len(f.read()) / 1024
    check("File Size", file_size_k, 256)
    check("Codec", stream["codec_name"], "vp9")
    check("Streams (no audio)", len(info["streams"]), 1)

    sys.exit(0)

filters = []


if ns.crop is not None:
    filters.append("crop=%s:%s:%s:%s" % tuple(ns.crop))
    out_width = ns.crop[0]
    out_height = ns.crop[1]
else:
    out_width = width
    out_height = height

max_dim = max(out_width, out_height)

if max_dim > 512:
    scale = 512 / max_dim
    out_width = int(round(out_width * scale))
    out_height = int(round(out_height * scale))
    filters.append("scale=%sx%s" % (out_width, out_height))

if ns.time_scale is not None:
    pts = ns.time_scale
elif duration > 3:
    pts = 3 / duration
else:
    pts = 1

out_duration = duration * pts

if pts != 1:
    filters.append("setpts=%.3f*PTS" % pts)


out_filename = str(pathlib.Path("/tmp/") / (ns.file.stem + ".webm"))
print("Output")
print("\tPath: %s" % out_filename)
print("\tWidth: %s" % out_width)
print("\tHeight: %s" % out_height)
print("\tDuration: %s" % out_duration)


args = ["ffmpeg", "-i", filename]
if filters:
    args += ["-filter:v", ", ".join(filters)]

if ns.bitrate:
    args += ["-c:v", "libvpx-vp9", "-b:v", ns.bitrate]


args += [out_filename]


print("FFmpeg Command")
print("\t" + " ".join(map(shlex.quote, args)))

