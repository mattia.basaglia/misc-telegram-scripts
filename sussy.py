#!/usr/bin/env python3
from telethon.tl.functions.channels import EditBannedRequest
from telethon.tl.functions.users import GetFullUserRequest
from telethon.tl.types import ChatBannedRights, MessageActionChatAddUser, MessageActionChatJoinedByRequest, MessageActionChatJoinedByLink

from common import ClientCommmon


script = ClientCommmon()

script.parser.add_argument("chat_id", type=int)


def ban(client, user, chat):
    client(EditBannedRequest(
        chat,
        user,
        ChatBannedRights(
            datetime.datetime.now(),
            view_messages=True,
            send_messages=True,
            send_media=True,
            send_stickers=True,
            send_gifs=True,
            send_games=True,
            send_inline=True,
            embed_links=True
        )
    ))


def has_messages(client, chat, user):
    try:
        iterator = client.iter_messages(entity=chat, limit=1, from_user=user)
        while True:
            msg = next(iterator)
            if getattr(msg, "action", None).__class__ not in (MessageActionChatAddUser, MessageActionChatJoinedByRequest, MessageActionChatJoinedByLink):
                return True

    except StopIteration:
        return False


with script:
    chat = script.args["chat_id"]
    for user in script.client.iter_participants(entity=chat):
        if user.is_self:
            continue

        if user.username is not None:
            name = "@" + user.username + " "
        else:
            name = ""

        if user.first_name is not None:
            name += user.first_name


        if user.last_name is not None:
            name += " " + user.last_name

        if user.deleted:
            print(name)
            print("    Removing (deleted user)")
            ban(script.client, user, chat)
            continue

        if not has_messages(script.client, chat, user):
            indent = " " * 4
            print(name)

            print(indent + "No messages")
            if not user.photo:
                print(indent + "No pfp")

            full = script.client(GetFullUserRequest(user.id))
            #if not full.chats:
                #print(indent + "No chats in common")
            #else:
                #print(indent + "Chats in common:")
                #for chat in full.chats:
                    #print(indent * 2 + chat.title)

            if full.full_user.about:
                print(indent + "Bio: " + full.full_user.about)
            else:
                print(indent + "No Bio")
