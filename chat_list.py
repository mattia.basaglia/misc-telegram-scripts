#!/usr/bin/env python3
from common import ClientCommmon


script = ClientCommmon()
script.parser.add_argument("--number", "-n", type=int, default=20)
script.parser.add_argument("--search", "-s", default="")


with script:
    search = script.args["search"].lower()
    limit = script.args["number"]
    if search:
        count = 0
        for dialog in script.client.iter_dialogs(archived=False):
            if search in dialog.title.lower():
                count += 1
                print('{:>14}: {}'.format(dialog.id, dialog.title))
                if count >= limit:
                    break
    else:
        for dialog in script.client.iter_dialogs(limit=limit, archived=False):
            print('{:>14}: {}'.format(dialog.id, dialog.title))
