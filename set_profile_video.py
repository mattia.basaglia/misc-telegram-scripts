#!/usr/bin/env python3
from common import ClientCommmon
from telethon import functions, types


script = ClientCommmon()
script.parser.add_argument("file")


with script:
    result = script.client(functions.photos.UploadProfilePhotoRequest(
        video=script.client.upload_file(script.args["file"])
    ))
    breakpoint()

